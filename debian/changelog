rbootd (3.0) unstable; urgency=medium

  [ Helge Deller ]
  * Update copyright file
  * Merge Makefile.linux into Makefile
  * New Homepage & maintainer
  * Make build reproduceable (closes: Bug#776951)
  * Fix various lintian warnings
  * Drop trailing whitespaces in changelog
  * Fix debian-rules-missing-recommended-target warning
  * rbootd: Exit with return code 0 when terminated
  * Add systemd unit file
  * Convert to Debian native format
  * Drop trailing spaces in debian/rules file
  * Clean up rbootd init.d file, add status and LSB description
  * Add hardening flags
  * Fix copyright tag in debian/copyright, and wrong copyright statement for
    the main source code
  * Fix copying changelog file in debian/rules

 -- Helge Deller <deller@gmx.de>  Thu, 15 Oct 2020 23:35:39 +0200

rbootd (2.0-11) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer. (See #934433)
  * debian/control:
      - Bumped Standards-Version to 4.5.0.
      - Changed Priority from extra to optional.

 -- Paulo Henrique Hebling Correa <contato@p2hc.com.br>  Tue, 12 May 2020 22:26:12 -0300

rbootd (2.0-10) unstable; urgency=low

  * New-Maintainer upload (closes: Bug#465907)
  * Added support for nostrip build option (closes: Bug#437883)
  * Improved packaging
  * Let rbootd decide the interface to use (closes: Bug#474709)
  * Added support for default file with interface setting
  * Adjusted changelog
  * Code improvements
    . Avoid trigraph
    . Include <time.h> instead of <sys/time.h> for time()
    . Removed obfuscated C line that was supposed to be dropped in 2.0-4

 -- Martin Schulze <joey@infodrom.org>  Mon, 07 Apr 2008 17:20:32 +0200

rbootd (2.0-9) unstable; urgency=low

  * QA upload.
  * Set maintainer to Debian QA Group
  * Added LSB formatted dependency info in init.d script (closes: #469713)
  * Updated standards version

 -- Peter Eisentraut <petere@debian.org>  Wed, 02 Apr 2008 11:27:50 +0200

rbootd (2.0-8.1) unstable; urgency=low

  * Non-maintainer upload.
  * Use invoke-rc.d; closes: #367729.
  * Remove bashisms from debian/rules; closes: #379635.

 -- Robert S. Edmonds <edmonds@debian.org>  Sun, 02 Sep 2007 02:04:31 -0400

rbootd (2.0-8) unstable; urgency=low

  * Removed debian/changelog.{rej,orig}
  * Corrected PF_SOCKET to PF_PACKET in pcap.c

 -- Alan Bain <afrb2@debian.org>  Mon, 19 Sep 2005 22:18:57 +0100

rbootd (2.0-7) unstable; urgency=low

  * Changed to use PF_PACKET socket instead of SOCK_PACKET
  * updated copyright to point to common-licenses
  * removed /usr/doc symlink creation
  * updated debian standards

 -- Alan Bain <afrb2@debian.org>  Tue, 13 Sep 2005 23:20:23 +0100

rbootd (2.0-6.1) unstable; urgency=low

  * Non maintainer upload
  * Rebuilt with new libpcap to remove dependency on libpcap0, which I
    got removed from unstable by accident. Sorry about this...

 -- Torsten Landschoff <torsten@debian.org>  Sat, 10 Aug 2002 11:37:29 +0200

rbootd (2.0-6) unstable; urgency=low

  * Added build-depends on libpcap-dev closing bug #84507
  * Corrected upstream source files

 -- Alan Bain <afrb2@debian.org>  Tue, 3 Jul 2001 16:00:00 +0100

rbootd (2.0-5) unstable; urgency=low

  * Fixed a mistake in the control file
  * Package (2.0-3) didn't print a warning when upgraded to 2.0-4;
    this has now been corrected.
  * README no longer refers to /usr/local/lib
  * URL in README updated (closing bug #49456)
  * Package now suggests bootparamd and nfs-server rather than
    netstd (closing bug #49384)

 -- Alan Bain <afrb2@debian.org>  Sat, 4 Nov 1999 20:00:00 +0000

rbootd (2.0-4) unstable; urgency=low

  * Updated the control file to cover PA-RISC workstations
  * Fixed a potential buffer overrun problem
  * Incorporated a patch from Peter Maydell removing obfuscated C line
  * Corrected an error in the manpage
  * Removed a skeleton file accidentally left in distribution
  * Removed creation of /usr/local/lib/rbootd directory
  * postinst now displays a message about the move
  * Moved documentation to /usr/share directories

 -- Alan Bain <afrb2@debian.org>  Tue, 23 Nov 1999 14:05:00 +0000

rbootd (2.0-3.2) unstable; urgency=low

  * Non-maintainer release
  * (Joey Added rbootd-singlefile.diff from The Puffin Group / PA-RISC
    porters from the srpm as found at
    ftp://puffin.external.hp.com/pub/parisc/binaries/SRPMS/rbootd-2.0-2.src.rpm

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Mon, 25 Oct 1999 02:41:10 +0200

rbootd (2.0-3.1) unstable; urgency=low

  * Non-maintainer release
  * Extended changelog
  * Changed from /usr/local/lib/rbootd to /var/lib/rbootd (closes:
    Bug#47618)
  * Added that directory to .deb files

 -- Martin Schulze <joey@finlandia.infodrom.north.de>  Sun, 24 Oct 1999 19:23:12 +0200

rbootd (2.0-3) unstable; urgency=low

  * Added README file provided by Peter Maydell which gives
    useful instructions on how to set up rbootd on a debian
    system.

 -- Alan Bain <afrb2@cam.ac.uk>  Tue, 2 Feb 1999 18:00:00 +0000

rbootd (2.0-2) unstable; urgency=low

  * Changed init script name to rbootd rather than rbootd_init
  * Moved bootfile location to /usr/local/lib/rboot rather than
    /export/hp/root (!)
  * compile options now include -g
  * strip binary in situ rather than before installing
  * sequence no is 20 rather than 35
  * copyright now refers to BSD copyright
  * capitalisation fixed
  * manpage paths fixed

 -- Alan Bain <afrb2@cam.ac.uk>  Sun, 21 Jun 1998 01:30:00 +0100

rbootd (2.0-1) unstable; urgency=low

  * Created package for first time from files on Peter Maydell's
    website http://www.chiark.greenend.org.uk/~pmaydell
  * Created scripts to start and stop rbootd with a null config
    file to start with.

 -- Alan Bain <afrb2@cam.ac.uk>  Mon, 15 Jun 1998 10:00:00 +0100

Local variables:
mode: debian-changelog
End:
