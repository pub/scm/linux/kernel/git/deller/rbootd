UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)

PROG=	rbootd
SRCS=	pcap.c conf.c parseconf.c rbootd.c rmpproto.c utils.c
OBJS =  pcap.o conf.o parseconf.o rbootd.o rmpproto.o utils.o
MAN=	rbootd.8

# We used to -lbsd but that seems to have gone away with glibc...
LFLAGS = -lpcap

# -Wall picks up all the static char rcsid[] stuff but is otherwise clean
CFLAGS = -O2 -Wall -Wno-unused-variable
rbootd:	$(OBJS)
	$(CC) -o $@ $(OBJS) $(LFLAGS)

clean:
	rm -f *.o
	rm -f rbootd
	rm -f rbootd.8.gz

rbootd.8.gz: $(MAN)
	gzip -9 < rbootd.8 > rbootd.8.gz

all: rbootd rbootd.8.gz

INSTALL = install
IFLAGS  = -o root -g root
INSTALL += ${IFLAGS}
DESTDIR = ./root

install: all
	${INSTALL} -d ${DESTDIR}/etc \
		${DESTDIR}/usr/sbin \
		${DESTDIR}/var/lib/rbootd \
		${DESTDIR}/usr/share/man/man8
	${INSTALL} -m644 etc-rbootd/rbootd.conf ${DESTDIR}/etc
	${INSTALL} -m755 rbootd ${DESTDIR}/usr/sbin
	${INSTALL} -m444 rbootd.8.gz ${DESTDIR}/usr/share/man/man8

else # BSD

# Makefile for BSD

PROG=	rbootd
SRCS=	bpf.c conf.c parseconf.c rbootd.c rmpproto.c utils.c
MAN=	rbootd.8

.include <bsd.prog.mk>

endif
